const request = require('request');

const setLoginCookie = (storage, callback) => {
    const username = storage.get('username');
    const password = storage.get('password');

    request.post({
        url: 'https://android.exs.lv/auth/login',
        form: { username, password }
    }, (error, response, body) => {
        const cookie = response.headers['set-cookie'];
        const data = JSON.parse(body);

        storage.set('cookie', cookie);

        console.log('Logged in as', username);
        callback();
    });
};

const getToken = (storage, callback) => {
    const cookie = storage.get('cookie');

    request.get({
        url: 'https://android.exs.lv/auth/gettoken',
        headers: { cookie }
    }, (error, response, body) => {
        const data = JSON.parse(body);

        storage.set('xsrf', data.xsrf);
        callback();
    });
};

module.exports = (storage) => {
    const xsrf = (req, res, next) => {
        let cookie = storage.get('cookie', false);

        if (!cookie) {
            setLoginCookie(storage, () => {
                getToken(storage, next);
            });
        } else {
            getToken(storage, next);
        }
    };

    return {
        xsrf
    };
};

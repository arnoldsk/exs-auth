class Storage {
    constructor() {
        this.data = {};
    }

    get(key, fallback = null) {
        if (typeof this.data[key] !== 'undefined') {
            return this.data[key];
        }
        return fallback;
    }

    set(key, value) {
        this.data[key] = value;
    }
}

module.exports = new Storage();

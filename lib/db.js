const mysql = require('mysql2/promise');
const code = require('./code');

module.exports = (() => {
    this.instance = null;

    const getInstance = async () => {
        if (!this.instance) {
            this.instance = await mysql.createConnection({
                host: 'localhost',
                user: 'root',
                password: 'root',
                database: 'exs'
            });
        }

        return this.instance;
    };

    const getUser = async (userId) => {
        try {
            const db = await this.getInstance();
            const [rows, fields] = await db.execute('SELECT * FROM `users` WHERE `id` = ?', [userId]);

            return rows.length ? rows[0] : null;
        } catch(error) {
            return null;
        }
    };

    const addUser = async (userId) => {
        try {
            const code = code();
            const db = await this.getInstance();
            await db.execute('INSERT INTO `users` SET `id` = ?, `code` = ?', [userId, code]);

            return {
                userId,
                code,
            };
        } catch(error) {
            return false;
        }
    };

    return {
        getInstance,
        getUser,
        addUser,
    };
})();

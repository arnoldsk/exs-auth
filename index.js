
require('dotenv').config();
const request = require('request');
const express = require('express');
const app = express();

const storage = require('./lib/storage');
const db = require('./lib/db');

/**
 * Config
 */
storage.set('username', process.env.USERNAME);
storage.set('password', process.env.PASSWORD);

/**
 * Middlewares
 */
const middlewares = require('./lib/middlewares')(storage);

/**
 * Routes
 */
app.get('/', (req, res) => {
    res.json({
        data: {
            code: code(),
        }
    });
});

app.get('/messages', middlewares.xsrf, (req, res) => {
    const cookie = storage.get('cookie');
    const xsrf = storage.get('xsrf');

    request.post({
        url: `https://android.exs.lv/inbox/received?xsrf=${xsrf}`,
        headers: { cookie }
    }, (error, response, body) => {
        const data = JSON.parse(body);

        res.json(data);
    });
});

app.get('/read/:id', middlewares.xsrf, (req, res) => {
    const cookie = storage.get('cookie');
    const xsrf = storage.get('xsrf');

    request.post({
        url: `https://android.exs.lv/inbox/read/${req.params.id}?xsrf=${xsrf}`,
        headers: { cookie }
    }, (error, response, body) => {
        const data = JSON.parse(body);

        res.json(data);
    });
});

app.get('/register/:id', middlewares.xsrf, async (req, res) => {
    const userId = req.params.id;
    const user = await db.getUser(userId);

    return;

    const cookie = storage.get('cookie');
    const xsrf = storage.get('xsrf');

    request.post({
        url: `https://android.exs.lv/inbox/send?xsrf=${xsrf}`,
        headers: { cookie },
        form: {
            msg_to: req.params.id,
            msg_title: 'Ziņa no kodu vīra',
            msg_content: `
                <p>Tavs kods ir: <strong>${code()}</strong></p>
                <p style="font-size:90%;color:#888">Šī ziņa ir nosūtīta automātiski.</p>
            `
        }
    }, (error, response, body) => {
        const data = JSON.parse(body);

        res.json(data);
    });
});

app.listen(3000, () => console.log('Connected!'));
